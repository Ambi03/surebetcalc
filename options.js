// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

'use strict';
// Saves options to chrome.storage
function save_options() {
  var betSize = document.getElementById('betSize').value;
    chrome.storage.sync.set({
        'betSize': betSize
    }, function () {
        // Update status to let user know options were saved.
        var status = document.getElementById('status');
        status.textContent = 'New bet size: ' + betSize + ' saved.';
        setTimeout(function () {
            status.textContent = ' ';
        }, 2000);
    });
}

// Saves options to chrome.storage
function save_options_partial() {
    var betSize = document.getElementById('betSizePartial').value;
    chrome.storage.sync.set({
        'betSizePartial': betSize
    }, function () {
        // Update status to let user know options were saved.
        var status = document.getElementById('status');
        status.textContent = 'New minimal bet size: ' + betSize + ' saved.';
        setTimeout(function () {
            status.textContent = ' ';
        }, 2000);
    });
}

// Saves options to chrome.storage
function save_options_minimal_odd() {
    var minimalOdd = document.getElementById('minimalOdd').value;
    chrome.storage.sync.set({
        'minimalOdd': minimalOdd
    }, function () {
        // Update status to let user know options were saved.
        var status = document.getElementById('status');
        status.textContent = 'New minimal odd size: ' + minimalOdd + ' saved.';
        setTimeout(function () {
            status.textContent = ' ';
        }, 2000);
    });
}

function init_betSizeInput_value() {
	var betSizeInput = document.getElementById('betSize');
	chrome.storage.sync.get('betSize', function(result) {
		var betSizeValue = result.betSize;
		if (betSizeValue) {
			betSizeInput.value = betSizeValue;
		}
	});

    var betSizeInputPartial = document.getElementById('betSizePartial');
    chrome.storage.sync.get('betSizePartial', function(result) {
        var betSizeValuePartial = result.betSize;
        if (betSizeValuePartial) {
            betSizeInputPartial.value = betSizeValuePartial;
        }
    });
}

document.getElementById('save').addEventListener('click', save_options);
document.getElementById('savePartial').addEventListener('click', save_options_partial);
document.getElementById('saveMinimalOdd').addEventListener('click', save_options_minimal_odd);
init_betSizeInput_value();
