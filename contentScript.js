// Add bubble to the top of the page.
var bubbleDOM = document.createElement('div');
bubbleDOM.setAttribute('class', 'selection_bubble');
document.body.appendChild(bubbleDOM);

// create betSize holder
function createBetSizeHolder() {
    var betSizeHolder = document.createElement('input');
    betSizeHolder.id = 'betSizeHolder';
    betSizeHolder.type = 'number';
    betSizeHolder.style.visibility = 'hidden';

    return betSizeHolder;
}

// create betSize holder
function createBetSizeHolderPartial() {
    var betSizeHolder = document.createElement('input');
    betSizeHolder.id = 'betSizeHolderPartial';
    betSizeHolder.type = 'number';
    betSizeHolder.style.visibility = 'hidden';

    return betSizeHolder;
}

// create betSize holder
function createMinimalOddHolder() {
    var minimalOddHolder = document.createElement('input');
    minimalOddHolder.id = 'minimalOddHolder';
    minimalOddHolder.type = 'number';
    minimalOddHolder.style.visibility = 'hidden';

    return minimalOddHolder;
}

function getBetSizeHolder() {
    return document.getElementById('betSizeHolder');
}

function getBetSizeHolderPartial() {
    return document.getElementById('betSizeHolderPartial');
}

function getMinimalOddHolder() {
    return document.getElementById('betSizeHolderPartial');
}

document.body.appendChild(createBetSizeHolder());
document.body.appendChild(createBetSizeHolderPartial());
document.body.appendChild(createMinimalOddHolder());

chrome.storage.sync.get('betSize', function(result) {
    var betSizeValue = result.betSize;
    var betSizeToSet = betSizeValue ? betSizeValue : 3000;

    getBetSizeHolder().value = betSizeToSet;
})

chrome.storage.sync.get('betSizePartial', function(result) {
    var betSizeValue = result.betSize;
    var betSizeToSet = betSizeValue ? betSizeValue : 3000;

    getBetSizeHolderPartial().value = betSizeToSet;
})

chrome.storage.sync.get('minimalOdd', function(result) {
    var minimalOddValue = result.betSize;
    var minimalOddValueToSet = minimalOddValue ? minimalOddValue : 1.01;

    getMinimalOddHolder().value = minimalOddValueToSet;
})

class YieldWithDescription {
    yieldSize;
    description;
    constructor(yieldSize, description) {
        this.yieldSize = yieldSize;
        this.description = description;
    }

    toString() {
        return this.yieldSize.toFixed(2) + " (" + this.description + ")";
    }
}

function extracted(probTrio, probOppositeDueto, description) {
    var totalProb = probTrio + probOppositeDueto;
    var minOdd = Math.min(1/probTrio, 1/probOppositeDueto);
    var minOddLimit = getMinimalOddHolder().value
    console.log("Minimalni pripustny kurz je " + minOddLimit)
    let yieldSize = minOdd < minOddLimit ? 0 : (1 - 1 / totalProb) * 100;
    return new YieldWithDescription(yieldSize, description);
}

// Lets listen to mouseup DOM events.
document.addEventListener('mouseup', function (e) {
    var selection = window.getSelection().toString() + "";
    if (selection.length > 0) {
        var odds = selection.trim().split(/\s+/);
		console.log("Selekce je [" + odds + "], delka je " + odds.length);	
        var i = 0;

        var totalProb = 0;
        var divisors = [];
        var probs = [];
        for (; i < odds.length && i < 6; i++) {
			//we have to normalize number formatting
			var oddWithDot = odds[i].replace(/,/g, '.');
			
            if (!isNaN(oddWithDot)) {
                var divisor = parseFloat(oddWithDot);
                if (divisor > 0) {
                    let prob = 1 / divisor;
                    totalProb += prob;
                    divisors.push(divisor);
                    probs.push(prob);
                }
            } 
        }

        // Fortuna speciality
        var yieldResults = [];
        var yieldResultString = "";
        if (probs.length === 6) {
            // 1 - 02
            yieldResults.push(extracted(probs[0], probs[4], "1x02"));
            // 0 - 12
            yieldResults.push(extracted(probs[1], probs[5], "0x12"));
            // 2 - 10
            yieldResults.push(extracted(probs[2], probs[3], "2x10"));

            // odfiltruju nuly
            yieldResults = yieldResults.filter(item => item.yieldSize > 0);
            // seradim podle vyteznosti
            yieldResults.sort((a,b) => a.yieldSize - b.yieldSize);

            // pripravim si label
            yieldResultString = "Vyteznosti: " + yieldResults.join(" | ") + "<br>";
        }

        if (totalProb > 1) {
            var betSize = getBetSizeHolder().value;
            var yieldResult = 1 - 1 / totalProb;
            var loss = yieldResult * betSize;
            var toBet = betSize - loss;

            var bets = divisors.map(x => (toBet / x).toFixed(2));
            var minimalBet = Math.min(... bets);

            var multiplier = 100 / minimalBet;
            var minimalAllowedBet = getBetSizeHolderPartial().value;
            var newBets = bets.map(x => (x * multiplier).toFixed(2));

            let singleYieldResultString = "Vyteznost: " + (yieldResult * 100).toFixed(2) + "%<br> ";
            renderBubble(e.clientX, e.clientY,
                "<p>" + (!yieldResultString || yieldResultString.length === 0 ? singleYieldResultString : yieldResultString) +
                "Ztrata: " + loss.toFixed(2) + "<br>" +
                "Sazky: " + bets.join(' | ') + "</p>" +
                "Sazky nove: " + newBets.join(' | ') + "</p>" +
                "Multiplier: " + multiplier);
			console.log("Vykresluji bublinu pro " + odds);
        }
    }
}, false);


// Close the bubble when we click on the screen.
document.addEventListener('mousedown', function (e) {
    bubbleDOM.style.visibility = 'hidden';
}, false);

// Move that bubble to the appropriate location.
function renderBubble(mouseX, mouseY, selection) {
    bubbleDOM.innerHTML = selection;
    bubbleDOM.style.top = mouseY + 'px';
    bubbleDOM.style.left = mouseX + 'px';
    bubbleDOM.style.visibility = 'visible';
}
